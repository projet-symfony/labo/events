<?php

namespace App\Form;

use App\Entity\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('name', null, [
            'label' => 'Nom de l\'événement:',
            'attr' => ['autofocus' => true]])
        ->add('location',null,[
            'label' => 'Localisation:'
        ])
        ->add('price', null, [
            'html5' => true,
            'scale' => 2,
            'label' => 'prix d\'entrée',
            'attr' => ['step' => 0.01]])
        ->add('capacity',null,[
            'label' => 'Capacité:',
            'empty_data' => '1'
        ])
        ->add('imageFileName',null,[
            'label' => 'Image:',
            'empty_data' => 'placeholder.jpg'
        ])
        ->add('description',null,[
            'label' => 'Description:'
        ])
        // ->add('startAt', DateTimeType::class)
        ->add('startAt',null,[
            'label' => 'Date de l\'événement:'
        ])
        ->getForm()

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
