<?php

namespace App\Controller;

use App\Entity\Event;
use App\Form\EventFormType;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EventsController extends AbstractController
{
    /**
     * Display coming up event
     */
    #[Route('/', name: 'app_home', methods: ['GET'])]
    #[Route('/events', name: 'app_events', methods: ['GET'])]
    public function index(EventRepository $eventRepository): Response
    {
        // $events = $eventRepository->findBy([],['startAt' => 'DESC']);
        $events = $eventRepository->findComingUpOrderByAscStartAt();

        return $this->render('events/index.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * Display past Event
     */
    #[Route('/PastEvents', name: 'app_pastEvents', methods: ['GET'])]
    public function indexx(EventRepository $eventRepository): Response
    {
        // $events = $eventRepository->findBy([],['startAt' => 'DESC']);
        $events = $eventRepository->findPastEventsOrderByAscStartAt();

        return $this->render('events/index.html.twig', [
            'events' => $events,
        ]);
    }

    /**
     * Create new event
     */
    #[Route('events/create', name: 'app_create', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $event = new Event;

        $form = $this->createForm(EventFormType::class, $event);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($event);
            $em->flush();

            $this->addFlash('success','Event created successfully !');

            return $this->redirectToRoute('app_show',['id' => $event->getId()]);
        };
        return $this->renderForm('events/create.html.twig',[
            'form' => $form
        ]);
    }

    /**
     * Show detail event
     */
    #[Route('/events/{id<[0-9]+>}', name: 'app_show', methods: ['GET'])]
    public function show(Event $event): Response
    {
        return $this->render('events/show.html.twig',[
            'event' => $event
        ]);
    }

    /**
     * Edit event
     */
    #[Route('/events/{id<[0-9]+>}/edit', name: 'app_edit', methods: ['GET','POST'])]
    public function edit(Event $event, Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(EventFormType::class, $event);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->flush();

            $this->addFlash('success','Event updated successfully !');

            return $this->redirectToRoute('app_show',['id' => $event->getId()]);
        };

        // return $this->render('events/edit.html.twig',[
        //     'event' => $event,
        //     'form' => $form->createView()
        // ]);
        return $this->renderForm('events/edit.html.twig',[
            'event' => $event,
            'form' => $form
        ]);
    }

    /**
     * Delete Event
     */
    #[Route('/events/{id<[0-9]+>}/delete', name: 'app_delete', methods: ['POST'])]
    public function delete(Request $request, Event $event, EntityManagerInterface $em): Response
    {
        if($this->isCsrfTokenValid('token_delete_event_' . $event->getId(), $request->request->get('csrf_token'))){
            $em->remove($event);
            $em->flush();

            $this->addFlash('danger', $event->getName() . ' deleted successfully !');
        }

        return $this->redirectToRoute('app_events');
    }
}