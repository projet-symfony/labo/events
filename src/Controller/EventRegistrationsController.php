<?php

namespace App\Controller;

use App\Entity\Event;
use App\Entity\Registration;
use App\Form\RegistrationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class EventRegistrationsController extends AbstractController
{
    /**
     * 
     */
    #[Route('events/{id<[0-9]+>}/registrations', name: 'app_registrations', methods: ['GET'])]
    public function index(Event $event): Response
    {
        $registrations = $event->getRegistrations();

        return $this->render('events/registrations/index.html.twig',[
            'registrations' => $registrations,
            'event' => $event
        ]);
    }

    /**
     * Create registration
     */
    #[Route('event/{id<[0-9]+>}/registration/create', name: 'app_registration_create', methods: ['GET', 'POST'])]
    public function create(Event $event, Request $requset, EntityManagerInterface $em): Response
    {
        if ($event->IsSoldeOut()){
            throw new AccessDeniedHttpException(sprintf('The event #%d is Sold Out !', $event->getId()));
        }

        $registration = new Registration;

        $form = $this->createForm(RegistrationFormType::class, $registration);

        $form->handleRequest($requset);

        if($form->isSubmitted() && $form->isValid()){
            $registration->setEvent($event);

            $em->persist($registration);
            $em->flush();

            $this->addFlash('success', "Thanks, you're registred !");

            return $this->redirectToRoute('app_registrations', ['id' => $event->getId()]);
        }

        return $this->render('events/registrations/create.html.twig',[
            'event' => $event,
            'form' => $form->createView()
        ]);
    }
}
