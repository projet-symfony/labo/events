<?php

namespace App\Twig;

use Twig\TwigFilter;
use App\Entity\Event;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\TwigFunction;
use Twig\Extra\Intl\IntlExtension;
use Twig\Extension\AbstractExtension;
// use Symfony\Component\Routing\RouterInterface;

class AppExtention extends AbstractExtension
{
    // public function __construct(private IntlExtension $intlExtension){}
    public function __construct(private IntlExtension $intlExtension, private UrlGeneratorInterface $urlGenerator){}

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('datetime', [$this, 'formatDateTime']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('format_price', [$this, 'formatPrice'], ['is_safe' => ['html']]),
            new TwigFunction('register_link_or_solde_out', [$this, 'registerLinkOrSoldeOut'], ['is_safe' => ['html']]),

        ];
    }

    public function pluralize(int $count, string $singular, string $plurial = null): string
    {
        $plurial = $plurial ?? $singular . 's';
        $string = $count <= 1 ? $singular : $plurial;

        return "$count $string";
    }

    public function formatPrice(Event $event): string
    {
        return $event->is_free() 
        ? '<b>Free</b>' 
        : $this->intlExtension->formatCurrency($event->getPrice(), 'EUR');
        // : (new intlExtension)->formatCurrency($event->getPrice(), 'EUR');
    }  

    public function formatDateTime(\DateTimeInterface $datetime): string
    {
        return $datetime->format('F d, Y \a\t H:i:s');
    }

    public function registerLinkOrSoldeOut(Event $event): string
    {
        
                if ($event->IsSoldeOut()){
                    return '<p class="btn btn-danger text-uppercase mb-3">⛔ &nbsp;&nbsp;&nbsp;&nbsp;Solde out !</p>';

                }else{

                    return sprintf('<a href="%s" class="btn btn-primary text-uppercase mb-3">📅 &nbsp;&nbsp;&nbsp;&nbsp;Register to this event
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">' .
                            $event->getSpotsLeft() .
                        '</span>
                    </a>', $this->urlGenerator->generate('app_registration_create', ['id' => $event->getId()]));
                }
    }
}

// return '<a href="{{path('app_registration_create', {'id':event.id}) }}" class="btn btn-primary text-uppercase mb-3">📅 &nbsp;&nbsp;&nbsp;&nbsp;Register to this event
// <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
//     {{event.spotsLeft}}
// </span>
// </a>';
