<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="events")
 * @ORM\Entity(repositoryClass=EventRepository::class)
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom de l'événement est requis !")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="L'emplacement est requis !")
     */
    private $location;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     * @Assert\PositiveOrZero(message="Le prix ne peux pas être négatif !")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Noter une description !")
     * @Assert\Length(
     *      min = 25,
     *      minMessage = "{{ limit }} charactères minimum !"
     * )
     */
    private $description;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Assert\NotBlank(message="La date et l'heure de l'événement sont est requis !")
     * @Assert\GreaterThanOrEqual("+1 hour", message="l'heure doit être supérireur d'une heure à l'heure de saisie !")
     */
    private $startAt;

    /**
     * @ORM\Column(type="string", length=255, options={"default": "placeholder.jpg"})
     * @Assert\Regex("/^(.*\.(jpe?g|png))$/i", message="l'image doit être au format jpg, jepg ou png !")
     */
    private $imageFileName = 'placeholder.jpg';

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     * @Assert\Positive(message="Le nombre de place doiy être supérieur à 1 !")
     */
    private $capacity = 1;

    /**
     * @ORM\OneToMany(targetEntity=Registration::class, mappedBy="event", orphanRemoval=true)
     */
    private $registrations;

    public function __construct()
    {
        $this->registrations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeImmutable $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getImageFileName(): ?string
    {
        return $this->imageFileName;
    }

    public function setImageFileName(?string $imageFileName): self
    {
        $this->imageFileName = $imageFileName;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * @return Collection|Registration[]
     */
    public function getRegistrations(): Collection
    {
        return $this->registrations;
    }

    public function addRegistration(Registration $registration): self
    {
        if (!$this->registrations->contains($registration)) {
            $this->registrations[] = $registration;
            $registration->setEvent($this);
        }

        return $this;
    }

    public function removeRegistration(Registration $registration): self
    {
        if ($this->registrations->removeElement($registration)) {
            // set the owning side to null (unless already changed)
            if ($registration->getEvent() === $this) {
                $registration->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * Check if event is free
     */
    public function is_free(): bool
    {
       return $this->getPrice() == 0 || is_null($this->getPrice());
    }

    /**
     * Calcul the number of spots left
     */
    public function getSpotsLeft(): int
    {
        return $this->capacity - $this->registrations->count();
    }

    /**
     * Check if there are no more spots available for this event.
     */
    public function IsSoldeOut(): bool
    {
        return $this->getSpotsLeft() === 0;
    }
    
}
