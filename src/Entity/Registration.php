<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\RegistrationRepository;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=RegistrationRepository::class)
 * @ORM\Table(name="registrations")
 */
class Registration
{
    public const HOW_HEARD_OPTIONS = [
        'Facebook' => 'Facebook',
        'Twitter' => 'Twitter',
        'Blog post' => 'Blog post',
        'News letter' => 'News letter',
        'Search engine' => 'Search engine',
        'Friend/Coworker' => 'Friend/Coworker',
        'other' => 'Other'
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Votre nom est requis !")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Votre email est requis !")
     * @Assert\Email(message = "L'email '{{ value }}' n'est pas valide.")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Merci de choisir une option !")
     * @Assert\Choice(choices=Registration::HOW_HEARD_OPTIONS, message="Please chose a valid option !")
     */
    private $howHeard;

    /**
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="registrations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHowHeard(): ?string
    {
        return $this->howHeard;
    }

    public function setHowHeard(string $howHeard): self
    {
        $this->howHeard = $howHeard;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }
}
